// Sets the MongoDB Database options

module.exports = {
    local:
    {
        name: "user-map-local",
        url: "mongodb://localhost/FamilyTreeApp",
        port: 27017,
        'secret': 'findfamilyneveresier',
    },
};
