// Pulls Mongoose dependency for creating schemas
var mongoose = require('mongoose');
var crypto = require('crypto');
var Schema = mongoose.Schema;
var defaultPassword='thiendeptrai';

// Creates a User Schema. This will be the basis of how user data is stored in the db
var UserSchema = new Schema({
    id: { type: Number },
    username: { type: String },
    password: { type: String },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    gender: { type: String, required: true },
    age: { type: Number, required: true },
    favlang: { type: String, required: true },
    location: { type: [Number], required: true },
    status: { type: String },
    htmlverified: String,
    reference: [{
        motherId: { type: Number },
        fatherId: { type: Number },
        createdAt: { type: Date, default: Date.now },
        UpdatedAt: { type: Date, default: Date.now }
    }],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    imageUrl: { type: String },
    isEnable: { type: Number }
});

// Sets the created_at parameter equal to the current time
UserSchema.pre('save', function (next) {
    var user = this;
    var salt = crypto.randomBytes(128).toString('base64');
    crypto.pbkdf2(user.password, salt, 10000, 512, function (err, derivedKey) {
        now = new Date();
        this.updated_at = now;
        if (!this.created_at) {
            this.created_at = now
        }
        user.password = derivedKey;
        next();

    });
});

UserSchema.index({ location: '2dsphere' });

module.exports = mongoose.model('user', UserSchema);
