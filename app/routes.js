// Dependencies
var mongoose = require('mongoose');
var User = require('./model.js');
var jwt    = require('jsonwebtoken'); 


// Opens App Routes
module.exports = function (app) {
    // GET Routes
    // --------------------------------------------------------
    // Retrieve records for all users in the db
    app.get('/users', function (req, res) {

        // Uses Mongoose schema to run the search (empty conditions)
        var query = User.find({});
        query.exec(function (err, users) {
            if (err) {
                res.send(err);
            } else {
                res.json(users);
            }
        });
    });

    // POST Routes
    // --------------------------------------------------------
    // Provides method for saving new users in the db
    app.post('/users', function (req, res) {

        if (req.body.status == "death") {
            req.body.imageUrl = "\\public\\image\\Death.gif"
        }
        else {
            if (req.body.gender == "Male") {
                req.body.imageUrl = "\\public\\image\\Male.gif"
            }
            else
                if (req.body.gender == "Female") {
                    req.body.imageUrl = "\\public\\image\\Female.gif"
                }
        }
        // Creates a new User based on the Mongoose schema and the post 
        var newuser = new User(req.body);

        // New User is saved in the db.
        newuser.save(function (err) {
            if (err)
                res.send(err);
            else
                // If no errors are found, it responds with a JSON of the new user
                res.json(req.body);
        });
    });

    // Retrieves JSON records for all users who meet a certain set of query conditions
    app.post('/query/', function (req, res) {
        var lat = req.body.latitude;
        var long = req.body.longitude;
        var distance = req.body.distance;
        var male = req.body.male;
        var female = req.body.female;
        var other = req.body.other;
        var minAge = req.body.minAge;
        var maxAge = req.body.maxAge;
        var favLang = req.body.favlang;
        var reqVerified = req.body.reqVerified;
        var status = req.body.status;

        var query = User.find({});

        // ...include filter by Max Distance (converting miles to meters)
        if (distance) {

            // Using MongoDB's geospatial querying features. (Note how coordinates are set [long, lat]
            query = query.where('location').near({
                center: { type: 'Point', coordinates: [long, lat] },

                // Converting meters to miles. Specifying spherical geometry (for globe)
                maxDistance: distance * 1609.34, spherical: true
            });

        }

        if (male || female || other) {
            query.or([{ 'gender': male }, { 'gender': female }, { 'gender': other }]);
        }

        if (minAge) {
            query = query.where('age').gte(minAge);
        }

        if (maxAge) {
            query = query.where('age').lte(maxAge);
        }

        if (favLang) {
            query = query.where('favlang').equals(favLang);
        }
        //if status is non-death then 
        if (!status) {
            query = query.where('status').equals('alive');
        }
        
        // ...include filter for HTML5 Verified Locations
        if (reqVerified) {
            query = query.where('htmlverified').equals("Yep (Thanks for giving us real data!)");
        }

        // Execute Query and Return the Query Results
        query.exec(function (err, users) {
            if (err)
                res.send(err);
            else
                // If no errors, respond with a JSON of all users that meet the criteria
                res.json(users);
        });
    });
    app.get('/getUserById', function (req, res) {
        // Uses Mongoose schema to run the search (empty conditions)
        var query = User.find({});
        query.exec(function (err, users) {
            if (err) {
                res.send(err);
            } else {
                res.json(users);
            }
        });
    });
    // DELETE Routes (Dev Only)
    // --------------------------------------------------------
    // Delete a User off the Map based on objID
    app.delete('/users/:objID', function (req, res) {
        var objID = req.params.objID;
        var update = req.body;

        User.findByIdAndRemove(objID, update, function (err, user) {
            if (err)
                res.send(err);
            else
                res.json(req.body);
        });
    });

    app.post('/updateRef', function (req, res) {
        var userToUpdate = req.body;
        var query = ({ _id: req.body._id });
        User.update(query, {
              fatherId: userToUpdate.fatherId
            , motherId: userToUpdate.motherId
        }, function (err, doc) {
            if(err)
            {
               res.send(err);     
            }
            else{
                res.json(doc)
            }
        }); 
    });
    
app.post('/authenticate', function(req, res) {
  // find the user
  User.findOne({
    name: req.body.name
  }, function(err, user) {

    if (err) throw err;

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {

      // check if password matches
      if (user.password != req.body.password) {
        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
      } else {

        // if user is found and password is right
        // create a token
        var token = jwt.sign(user, app.get('superSecret'), {
          expiresInMinutes: 1440 // expires in 24 hours
        });

        // return the information including token as JSON
        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token
        });
      }   

    }

  });
});

};