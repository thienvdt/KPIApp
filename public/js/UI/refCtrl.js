var refCtrl = angular.module('refCtrl', ['geolocation', 'gservice']);
refCtrl.controller('refCtrl', function ($scope, $log, $http, $rootScope, geolocation, gservice,userService) {
    var pageTypeId = "updateRef";
    var distanceToFind = 100;
    $scope.formData = {};
    $scope.formData.longitude = -98.350;
    $scope.formData.latitude = 39.500;


    navigator.geolocation.getCurrentPosition(function (position) {
        $scope.formData.longitude = position.coords.longitude;
        $scope.formData.latitude = position.coords.latitude;
    });
    gservice.refresh($scope.formData.latitude, $scope.formData.longitude, true, pageTypeId);
    $rootScope.$on("clicked", function () {
        $scope.$apply(function () {
            $scope.formData.latitude = parseFloat(gservice.clickLat).toFixed(3);
            $scope.formData.longitude = parseFloat(gservice.clickLong).toFixed(3);
        });
    });
    $scope.queryUsers = function () {
        queryBody = {
            longitude: parseFloat($scope.formData.longitude),
            latitude: parseFloat($scope.formData.latitude),
            distance: parseFloat(distanceToFind),
            fatherId: $scope.formData.fatherId,
            motherId: $scope.formData.motherId
        };
        $http.post('/query', queryBody)
            .success(function (queryResults) {
                gservice.refresh(queryBody.latitude, queryBody.longitude, queryResults, pageTypeId);
                $scope.queryCount = queryResults.length;
            })
            .error(function (queryResults) {
                console.log('Error ' + queryResults);
            })
    };
    $scope.backFn = function () {
        window.history.go(-1)
    }
    google.maps.event.addDomListener($("#map"), 'click', function () {
        alert('Map was clicked!');
    });
    $rootScope.$on('upDateRefOnForm',function (event, markerData) {
        
        console.log(markerData);
    });

});