var app = angular.module('meanMapApp', ['treeCtrl','refCtrl','addCtrl', 'queryCtrl', 'headerCtrl', 'geolocation', 'gservice','userService', 'ngRoute'])
    .config(function($routeProvider){
        $routeProvider.when('/join', {
            controller:  'addCtrl',
            templateUrl: 'partials/addForm.html',
        }).when('/find', {
            controller: 'queryCtrl',
            templateUrl: 'partials/queryForm.html',
        }).when('/reference', {
            controller: 'refCtrl',
            templateUrl: 'partials/referenceForm.html',
        }).when('/viewastree', {
            controller: 'treeCtrl',
            templateUrl: 'partials/treeForm.html',
        }).otherwise({
            redirectTo:'/join'
        })
    });
