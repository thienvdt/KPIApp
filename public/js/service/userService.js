angular.module('userService', ['gservice'])
    .service('userService', function ($rootScope, $http, gservice) {
        var userService = {};
        userService.queryUser = function (queryBody) {
            $http.post('/query', queryBody)

                .success(function (queryResults) {

                    gservice.refresh(queryBody.latitude, queryBody.longitude, queryResults);

                    $scope.queryCount = queryResults.length;
                })
                .error(function (queryResults) {
                    console.log('Error ' + queryResults);
                });
        };
        userService.setCurrentUser = function (user) {
            userService.currentUser = user;
        };
        userService.setSelectedUser = function (user) {
            userService.selectedUser = user;
        };
        return userService;
    });

